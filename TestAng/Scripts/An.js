﻿/// <refernce path = "angular.js"/>

var myApp = angular.module("myModule", []);

myApp.controller("myController", function ($scope) {

    //var country = {
    //    name: "USA",
    //    capital : "Washington DC",
    //    flag: "/Images/american-flag-large.png"

    //};
    //$scope.country = country;

    //var employee = {
    //    firstname: "david",
    //    lastname: "Washington DC",
    //    gender: "male"

    //};
    //$scope.employee = employee;

    //var employee = 
    //    [
    //        { firstName: "Anish", lastName: "Ranjan", Gender: "Male", Salaray: 50000 },
    //        { firstName: "Gunin", lastName: "Waliya", Gender: "Male", Salaray: 80000 },
    //        { firstName: "Aditya", lastName: "Bakliwal", Gender: "Male", Salaray: 90000 },
    //        { firstName: "Nitika", lastName: "Rajendra", Gender: "Female", Salaray: 10000 },
    //        { firstName: "Rajat", lastName: "Patel", Gender: "Male", Salaray: 40000 },
    //        { firstName: "Akshit", lastName: "Shah", Gender: "Male", Salaray: 20000 }

    //    ];
    //$scope.employee = employee;

    //var countries = [
    //    {
    //        name: "UK",
    //        cities: [
    //            { name: "London" },
    //            { name: "Manchester" },
    //            {name: "Birmingham"}
    //        ]

    //    },
    //    {
    //        name: "USA",
    //        cities: [
    //            { name: "Kingsville" },
    //            { name: "Green Bay" },
    //            { name: "Reno" }
    //        ]

    //    },
    //    {
    //        name: "India",
    //        cities: [
    //            { name: "Mumbai" },
    //            { name: "Bengalore" },
    //            { name: "Delhi" }
    //        ]

    //    }
    //];

    //$scope.countries = countries;

    //var technologies = [

    //    { name: "C#", Likes: 0, Dislikes: 0 },
    //    { name: "Asp.Net", Likes: 0, Dislikes: 0 },
    //    { name: "AngularJs", Likes: 0, Dislikes: 0 },
    //    { name: "MVC", Likes: 0, Dislikes: 0 }

    //];

    //$scope.technologies = technologies;

    //$scope.incrementLikes = function (technology)
    //{
    //    technology.Likes++;
    //}

    //$scope.incrementDisLikes = function (technology) {
    //    technology.Dislikes++;
    //}

    //var employee = 
    //    [
    //        { firstName: "Anish", lastName: "Ranjan", dateofBirth: new Date("November 23, 1980"), Gender: "Male", Salaray: 50000 },
    //        { firstName: "Gunin", lastName: "Waliya", dateofBirth: new Date("May 21, 1982"), Gender: "Male", Salaray: 80000 },
    //        { firstName: "Aditya", lastName: "Bakliwal", dateofBirth: new Date("June 24, 1922"), Gender: "Male", Salaray: 90000 },
    //        { firstName: "Nitika", lastName: "Rajendra", dateofBirth: new Date("July 22, 1987"), Gender: "Female", Salaray: 10000 },
    //        { firstName: "Rajat", lastName: "Patel", dateofBirth: new Date("August 03, 1955"), Gender: "Male", Salaray: 40000 },
    //        { firstName: "Akshit", lastName: "Shah", dateofBirth: new Date("September 13, 1966"), Gender: "Male", Salaray: 20000 }

    //    ];
    //$scope.employee = employee;
    //$scope.rowLimit = 3;


    //var employee =
    //    [
    //        { firstName: "Anish", lastName: "Ranjan", dateofBirth: new Date("November 23, 1980"), Gender: "Male", Salaray: 50000 },
    //        { firstName: "Gunin", lastName: "Waliya", dateofBirth: new Date("May 21, 1982"), Gender: "Male", Salaray: 80000 },
    //        { firstName: "Aditya", lastName: "Bakliwal", dateofBirth: new Date("June 24, 1922"), Gender: "Male", Salaray: 90000 },
    //        { firstName: "Nitika", lastName: "Rajendra", dateofBirth: new Date("July 22, 1987"), Gender: "Female", Salaray: 10000 },
    //        { firstName: "Rajat", lastName: "Patel", dateofBirth: new Date("August 03, 1955"), Gender: "Male", Salaray: 40000 },
    //        { firstName: "Akshit", lastName: "Shah", dateofBirth: new Date("September 13, 1966"), Gender: "Male", Salaray: 20000 }

    //    ];
    //$scope.employee = employee;
    //$scope.sortColumn = "firstName";


    var employee =
        [
            { firstName: "Anish", lastName: "Ranjan", dateofBirth: new Date("November 23, 1980"), Gender: "Male", Salaray: 50000 },
            { firstName: "Gunin", lastName: "Waliya", dateofBirth: new Date("May 21, 1982"), Gender: "Male", Salaray: 80000 },
            { firstName: "Aditya", lastName: "Bakliwal", dateofBirth: new Date("June 24, 1922"), Gender: "Male", Salaray: 90000 },
            { firstName: "Nitika", lastName: "Rajendra", dateofBirth: new Date("July 22, 1987"), Gender: "Female", Salaray: 10000 },
            { firstName: "Rajat", lastName: "Patel", dateofBirth: new Date("August 03, 1955"), Gender: "Male", Salaray: 40000 },
            { firstName: "Akshit", lastName: "Shah", dateofBirth: new Date("September 13, 1966"), Gender: "Male", Salaray: 20000 }

        ];
    $scope.employee = employee;

    $scope.search = function (item) {
        if ($scope.sortEmp == undefined) {
            return true;
        }
        else {
            if (item.firstName.toLowerCase().indexof($scope.sortEmp.toLoweCase()) != -1 || item.lastName.toLowerCase().indexof($scope.sortEmp.toLoweCase()) != -1) {
                return true;
            }
        }

        return false;
    }
    //$scope.sortColumn = "firstName";
    //$scope.reverseSort = false;

    //$scope.sortData = function (column) {
    //    $scope.reverseSort = ($scope.sortColumn == column) ? !$scope.reverseSort : false;
    //    $scope.sortColumn = column;

    //}
    //$scope.getSortClass = function (column) {
    //    if ($scope.sortColumn == column) {
    //        return $scope.reverseSort ? 'arrow-down' : 'arrow-up';
    //    }
    //    return '';
    });